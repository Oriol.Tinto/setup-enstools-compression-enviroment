FROM ubuntu:latest

# Install some dependencies through apt
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt update \
    && apt install -yq vim make cmake wget git python3 python3-pip python3-venv \
    && apt install -yq swig gcc gfortran pkg-config libzstd-dev \
    && apt install -yq libproj-dev proj-data proj-bin libgeos-dev libeccodes-dev

# Copy recipes and set workdir
COPY dependencies workdir
WORKDIR workdir

# Install the filters + libpressio + enstools-compression
RUN make -j
