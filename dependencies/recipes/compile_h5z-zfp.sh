#!/bin/bash -l
set -xuve
source ../environment.sh
source etc/shared_functions.sh


# compile eccodes
SW=h5z-zfp
REPOSITORY=https://github.com/LLNL/H5Z-ZFP.git
VERSION=${H5Z_ZFP_VERSION}

INSTALLDIR=$(get_install_base)/${SW}/${VERSION}
echo ${INSTALLDIR}

mkdir -p ${INSTALLDIR}

# create temporal folder and switch into it
switch2build

TAG_NAME=v${VERSION}
# Clone repository and enter directory
git clone ${REPOSITORY} -b ${TAG_NAME} source

cd source

mkdir build
cd build 
cmake .. -DCMAKE_INSTALL_PREFIX=${INSTALLDIR}

make
make install

switch2base_cleanup
