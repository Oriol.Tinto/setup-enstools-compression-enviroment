# file with shared functions for creation of modules. 
#
# All functions write their results to stdout and to a Variable called RESULT.
RESULT=""

# clean up loaded module
function clean_modules() {
    unset LD_PRELOAD
    module purge
    module load spack
    module use /project/meteo-scratch/Oriol.Tinto/module_building/modulefiles
}

# base directory for installation, depends on site
function get_install_base() {
	RESULT=${INSTALLATION_DIR}
	echo $RESULT
}

# create a temporal folder for the compilation process
function create_tmp() {
    local tmp_path
    RESULT=$(pwd)/../build-module/${SW}/
    mkdir -p $RESULT
    echo $RESULT
}

# switch to temporal folder
function switch2build() {
    BASE_DIR=$PWD
    BUILD_DIR=$(create_tmp)
    cd $BUILD_DIR
}

# switch back to base dir and cleanup
function switch2base_cleanup() {
    cd $BASE_DIR
    rm -rf $BUILD_DIR
}
