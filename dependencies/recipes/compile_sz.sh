#!/bin/bash -l
set -xuve
source ../environment.sh
source etc/shared_functions.sh

# compile eccodes
SW=sz

VERSION=${SZ_VERSION}
REPOSITORY=https://github.com/szcompressor/SZ/

# Get path to the installation directory and create it
INSTALLDIR=$(get_install_base)/${SW}/${VERSION}
mkdir -p ${INSTALLDIR}

# create temporal folder and switch into it
switch2build

# Clone repository and enter directory
TAG_NAME=v${VERSION}
git clone ${REPOSITORY} -b ${TAG_NAME} source --depth 1

cd source



# Disable loading configuration file before compiling
sed "s/int load_conffile_flag = 1;/int load_conffile_flag = 0;/g" hdf5-filter/H5Z-SZ/src/H5Z_SZ.c -i

# Create build folder
mkdir build
cd build

# Launch cmake
CMAKE_FLAGS="-DCMAKE_INSTALL_PREFIX:PATH=${INSTALLDIR} -DBUILD_HDF5_FILTER:BOOL=ON"
cmake .. ${CMAKE_FLAGS}

# Compile and install
make
make install

# cleanup
switch2base_cleanup
