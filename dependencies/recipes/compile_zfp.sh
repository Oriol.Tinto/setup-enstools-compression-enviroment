#!/bin/bash -l
set -xuve
source ../environment.sh
source etc/shared_functions.sh


# compile eccodes
SW=zfp
VERSION=${ZFP_VERSION}
REPOSITORY=https://github.com/LLNL/zfp.git

# Get the path for the installation directory and create it
INSTALLDIR=$(get_install_base)/${SW}/${VERSION}
mkdir -p ${INSTALLDIR}

# create temporal folder and switch into it
switch2build

# Clone repository and enter directory
git clone ${REPOSITORY} -b ${VERSION} source --depth 1

cd source

# Create build directory and enter it
mkdir build
cd build

# Run CMake
cmake  -DCMAKE_INSTALL_PREFIX=${INSTALLDIR} -DZFP_BIT_STREAM_WORD_SIZE=8 .. 

# Compile
make
make install

# cleanup
switch2base_cleanup
