#!/bin/bash -l
set -xuve
source ../environment.sh
source etc/shared_functions.sh


# compile eccodes
SW=h5z-blosc
REPOSITORY=https://github.com/Blosc/hdf5-blosc.git

VERSION=${H5Z_BLOSC_VERSION}
INSTALLDIR=$(get_install_base)/${SW}/${VERSION}

mkdir -p ${INSTALLDIR}

# create temporal folder and switch into it
switch2build

# Clone repository and enter directory
TAG_NAME=v${VERSION}
TAG_NAME=master
git clone ${REPOSITORY} -b ${TAG_NAME} source

cd source
# compile and install
mkdir build
cd build

cmake .. -DCMAKE_INSTALL_PREFIX=${INSTALLDIR}

make
make install

# Somehow the Makefile doesn't copy these two required files
cp libblosc_filter.so ${INSTALLDIR}/lib
cp libH5Zblosc.so ${INSTALLDIR}/lib
