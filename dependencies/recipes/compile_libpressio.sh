#!/bin/bash -l
set -xuve
source ../environment.sh
source etc/shared_functions.sh


# compile eccodes
SW=libpressio
REPOSITORY=https://github.com/robertu94/libpressio.git
VERSION=${LIBPRESSIO_VERSION}


INSTALLDIR=$(get_install_base)/${SW}/${VERSION}
mkdir -p ${INSTALLDIR}


export CMAKE_PREFIX_PATH=${BLOSC_DIR}:${CMAKE_PREFIX_PATH:=""}:

# create temporal folder and switch into it
switch2build

# create and activate virtual environment (otherwise problems installing python module)
if [ ! -d venv ]; then
   python3 -m venv venv
   source venv/bin/activate
   python3 -m pip install numpy
fi

source venv/bin/activate


if [ ! -d source ]; then
   # Clone repository and enter directory
   git clone ${REPOSITORY} -b ${VERSION} source --depth 1
fi

cd source



########################################33
#
#   INSTALLATION
#

#FIXME: if we don't disable this doxygen flag compilation fails.
sed "s/set(CMAKE_SWIG_FLAGS \"-doxygen\")/#set(CMAKE_SWIG_FLAGS \"-doxygen\")/g" tools/swig/CMakeLists.txt -i

# Create build directory and go there
if [ ! -d build ]; then
   mkdir build
fi

cd build

# Run cmake
cmake ..  -DCMAKE_INSTALL_PREFIX=${INSTALLDIR} -DBUILD_PYTHON_WRAPPER=ON -DLIBPRESSIO_HAS_SZ=ON -DLIBPRESSIO_HAS_ZFP=ON -DLIBPRESSIO_HAS_BLOSC=ON -DCMAKE_CXX_FLAGS=-I${SZ_DIR}/include 

# Compile and install
make -j 
make install

PYTHON_INSTALL=../../venv/lib/python*/site-packages/
# Copy python files to python directory inside installdir
mkdir -p ${INSTALLDIR}/python

for file in libpressio.py pressio.py _pressio.so ; do
	cp ${PYTHON_INSTALL}/${file} ${INSTALLDIR}/python/
done

########################################33
