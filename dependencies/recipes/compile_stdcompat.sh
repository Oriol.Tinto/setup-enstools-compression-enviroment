#!/bin/bash -l
set -xuve
source ../environment.sh
source etc/shared_functions.sh


# compile eccodes
SW=std_compat
VERSION=${STD_COMPAT_VERSION}
REPOSITORY=https://github.com/robertu94/std_compat

INSTALLDIR=$(get_install_base)/${SW}/${VERSION}
mkdir -p ${INSTALLDIR}

# create temporal folder and switch into it
switch2build

# Clone repository and enter directory
git clone ${REPOSITORY} -b ${VERSION} source --depth 1

cd source

###########################################################
#
#  INSTALLATION
#

BUILD_DIR=build
mkdir $BUILD_DIR
cd $BUILD_DIR
cmake .. -DCMAKE_INSTALL_PREFIX=${INSTALLDIR}
make
make test
make install

###########################################################
# cleanup
switch2base_cleanup

