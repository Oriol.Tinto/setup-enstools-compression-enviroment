#!/bin/bash -l
# compiler HDF5 library
set -xuve
source ../environment.sh
source etc/shared_functions.sh
# clean_modules

# parallel or serial
SW=hdf5
# Version
VERSION=${HDF5_VERSION}

# Get tag name
TAG_NAME=hdf5-$( echo $VERSION | tr . _ )

INSTALLDIR=$(get_install_base)/${SW}/${VERSION}
mkdir -p ${INSTALLDIR}
REPOSITORY=https://github.com/HDFGroup/hdf5.git

# create temporal folder and switch into it
switch2build

# extract tar file
git clone ${REPOSITORY} -b ${TAG_NAME} source --depth 1

# enter the folder
cd source

# Create build directory and enter it
mkdir build
cd build

# Launch CMAKE
cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALLDIR -DHDF5_BUILD_FORTRAN=ON -DHDF5_BUILD_CPP_LIB=ON 

# compile and install
make -j $(nproc)
make install

# cleanup
switch2base_cleanup
