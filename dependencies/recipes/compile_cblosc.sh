#!/bin/bash -l
set -e
source ../environment.sh
source etc/shared_functions.sh

# compile eccodes
SW=c-blosc
REPOSITORY=https://github.com/Blosc/c-blosc.git
VERSION=${BLOSC_VERSION}


INSTALLDIR=$(get_install_base)/${SW}/${VERSION}
mkdir -p ${INSTALLDIR}


# create temporal folder and switch into it
switch2build

TAG_NAME=v${VERSION}
git clone ${REPOSITORY} -b ${TAG_NAME} source --depth 1

cd source

# compile and install
mkdir build
cd build

cmake -DCMAKE_INSTALL_PREFIX=${INSTALLDIR} ..

cmake --build .

ctest

cmake --build . --target install


# cleanup
switch2base_cleanup
