#!/bin/bash -l
set -xuve
source environment.sh

if [ ! -d enstools-compression ]; then
   # Clone enstools-compression
   git clone https://gitlab.physik.uni-muenchen.de/w2w/enstools-compression.git
fi

# Enter folder
cd enstools-compression

python3 -m pip install pytest pytest-mock
# Run tests
pytest tests
