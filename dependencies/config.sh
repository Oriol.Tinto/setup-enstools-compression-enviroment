# Versions that will be installed
HDF5_VERSION=1.13.1
SZ_VERSION=2.1.12.4
ZFP_VERSION=0.5.5
H5Z_ZFP_VERSION=1.1.0

BLOSC_VERSION=1.21.0
H5Z_BLOSC_VERSION=1.0.0
STD_COMPAT_VERSION=0.0.14
LIBPRESSIO_VERSION=0.83.0

# Name of the installation directory
INSTALLATION_DIR_NAME=dependencies
